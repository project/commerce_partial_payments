/**
 * @file
 * Dynamic updating of tracking totals.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  function getFormattedAmount(amount, settings, number_only) {
    let formatted = parseFloat(amount || 0)
      .toLocaleString(settings.locale, {
        style: 'currency',
        currency: settings.currency,
        currencyDisplay: number_only ? 'code' : 'symbol',
        useGrouping: false
      });

    if (number_only) {
      return (amount < 0 ? '-' : '') + formatted.substr(formatted.search('\u00A0| ') + 1);
    }
    else {
      return formatted;
    }
  }

  Drupal.behaviors.commerce_partial_payments_tracking_total = {
    attach: function (context) {
      $.each(drupalSettings.commerce_partial_payments.forms, function (selector, settings) {
        const $form = $(selector, context);
        const $total = $form.find('.cpp-tracking-total');
        const $items = $form.find('.cpp-tracking-amounts .form-type-commerce-number :input');
        const $warning = $form.find('.cpp-tracking-warning');

        $items.change(function (e) {
          // Consistently format the number.
          this.value = getFormattedAmount(this.value, settings, true);

          // Get the total that has been tracked.
          let total = 0;
          $items.each(function (index) {
            total += parseFloat(this.value || 0);
          });

          // Update the total.
          $total.html(getFormattedAmount(total, settings, false));

          // If a warning is configured, show it.
          if (typeof settings.warning !== 'undefined') {
            let warn;

            // Get our threshold has a number.
            const warning_amount = parseFloat(settings.warning);

            // 'exact' means it must be an exact match. To avoid float point
            // precision errors, format both as fixed strings.
            if (settings.warning_type === 'exact') {
              warn = total.toFixed(6) !== warning_amount.toFixed(6);
            }
            // Otherwise we assume an upper limit.
            else {
              warn = total > warning_amount;
            }

            // Show or hide the warning as required.
            if (warn) {
              $warning.removeClass('hidden');
            }
            else {
              $warning.addClass('hidden');
            }
          }
        });
      });
    }
  }

})(jQuery, Drupal, drupalSettings);
