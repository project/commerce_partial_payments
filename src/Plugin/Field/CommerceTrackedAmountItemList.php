<?php

namespace Drupal\commerce_partial_payments\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Defines a item list class for the commerce_tracked_amount field type.
 */
class CommerceTrackedAmountItemList extends EntityReferenceFieldItemList {

  /**
   * Get the tracked amounts in the field.
   *
   * @return \Drupal\commerce_price\Price[]
   *   The tracked amounts, keyed by target id.
   */
  public function getTrackedAmounts() {
    $tracking = [];
    foreach ($this->list as $item) {
      /** @var \Drupal\commerce_partial_payments\Plugin\Field\FieldType\CommerceTrackedAmountItem $item */
      $tracking[$item->target_id] = $item->toPrice();
    }
    return $tracking;
  }

}
