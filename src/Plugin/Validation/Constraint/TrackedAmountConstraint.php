<?php

namespace Drupal\commerce_partial_payments\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a constraint for the tracked amounts.
 *
 * @Constraint(
 *   id = "TrackedAmounts",
 *   label = @Translation("Tracked amounts", context = "Validation"),
 * )
 */
class TrackedAmountConstraint extends Constraint {

  /**
   * Error when there are not enough entries.
   *
   * @var string
   */
  public $error = 'Tracked amounts do not match the payment total.';

}
