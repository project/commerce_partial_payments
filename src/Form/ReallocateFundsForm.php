<?php

namespace Drupal\commerce_partial_payments\Form;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_partial_payments\Element\TrackedAmounts;
use Drupal\commerce_partial_payments\OrderItemTrackingInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Reallocate payments.
 *
 * @package Drupal\commerce_partial_payments\Form
 */
class ReallocateFundsForm extends FormBase implements TrustedCallbackInterface {

  use TrackingElementTrait;

  /**
   * The order item tracking service.
   *
   * @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface
   */
  protected $paymentTracking;

  /**
   * The payment being modified.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $class = new static(
      $container->get('commerce_partial_payments.order_item_tracking'),
      $container->get('entity_type.manager')
    );
    $class->setCurrencyFormatter($container->get('commerce_price.currency_formatter'));
    return $class;
  }

  /**
   * Construct the AddTrackingForm.
   *
   * @param \Drupal\commerce_partial_payments\OrderItemTrackingInterface $tracking
   *   The order item tracking service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(OrderItemTrackingInterface $tracking, EntityTypeManagerInterface $entity_type_manager) {
    $this->paymentTracking = $tracking;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_partial_payments_reallocate';
  }

  /**
   * Gets the current order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  protected function getOrder() : OrderInterface {
    return $this->getRouteMatch()->getParameter('commerce_order');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $order = $this->getOrder();
    $tracking_payments = $this->paymentTracking->getTrackedAmountsForOrder($order);

    if (count($tracking_payments) == 0) {
      // There are no payments, so just show a message.
      $this->messenger()->addWarning($this->t("This order doesn't have any payments, so funds can't be reallocated."));
      return $form;
    }

    $form['order_item_tracking'] = [
      '#type' => 'commerce_partial_payments_tracked_amount',
      // Need to pass the tracking payments into both properties as the "Paid"
      // column explicitly pulls from tracking_payment.
      '#tracking' => $tracking_payments,
      '#tracking_payment' => $tracking_payments,
      '#default_value' => [],
      '#items' => [],
      '#empty' => $this->t('This order has no items.'),
      '#allow_negative' => TRUE,
      '#header' => [
        'label' => '',
        'price' => $this->t('Price'),
        'tracked' => $this->t('Paid'),
        'balance' => $this->t('Balance'),
        'number' => $this->t('Refund'),
      ],
    ];

    // Add each item to the form. The element will add the amounts already paid.
    foreach ($order->getItems() as $item) {
      $form['order_item_tracking']['#items'][$item->id()] = [
        'label' => $item->label(),
        'price' => $item->getAdjustedTotalPrice(),
      ];
    }

    // Add a placeholder row for any order items that have been deleted so funds
    // can be correctly reallocated.
    foreach (array_keys($tracking_payments) as $item_id) {
      if (!isset($form['order_item_tracking']['#items'][$item_id])) {
        $form['order_item_tracking']['#items'][$item_id] = [
          'label' => $this->t("Deleted [@item]", ['@item' => $item_id]),
          'price' => $order->getTotalPrice()->multiply('0'),
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reallocate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_price\Price $total */
    $tracking_values = $form_state->getValue('order_item_tracking');
    $tracking_values = TrackedAmounts::extractValues($tracking_values, $total);

    if (empty($tracking_values)) {
      $form_state->setError($form['order_item_tracking'], $this->t('Please enter the amounts to reallocate.'));
    }
    elseif (!$total->isZero()) {
      $form_state->setError($form['order_item_tracking'], $this->t('For transfers within the same order the total of all the changes should be zero.'));
    }
    else {
      $this->payment = $this->entityTypeManager->getStorage('commerce_payment')->create([
        'payment_gateway' => 'reallocate_funds',
        'order_id' => $this->getOrder()->id(),
      ]);
      $this->payment->setAmount($total);
      $this->payment->set('order_item_tracking', $tracking_values);
      $this->payment->set('state', 'completed');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->payment->save();
    $this->messenger()->addStatus('Payments reallocated.');
    $form_state->setRedirect('entity.commerce_payment.collection', [
      'commerce_order' => $this->payment->getOrder()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['formatAmount'];
  }

}
